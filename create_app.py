"""
This project demonstrates Stack storage (LIFO) implementation with REST Web API Interface,

APIs required:
Push(string data) - adds text to stack
Pop() - pops item at the top of the stack
Peek() - shows most recent item without removing it
Revert() - Reverts the stack (first item becomes last and etc.)

Other things to note:
Make sure to persist the data.
Application overall quality should meet industry standards
Pay attention to performance.
Assume data.Length < 1kb


API is as follows:
|-----------------------------------------------------------------------------------------------------------|
| HTTP Method | URI                               | Action                                                  |
|-------------|-----------------------------------|---------------------------------------------------------|
|  GET        | http://[hostname]/stack           | Pop item from the top of the stack                      |
|  POST       | http://[hostname]/stack           | Push item to the top of the stack                       |
|  GET        | http://[hostname]/stack/peek      | Shows the item at the top of stack without removing it  |
|  GET        | http://[hostname]/stack/revert    | Reverses the stack                                      |
|-----------------------------------------------------------------------------------------------------------|
"""

import queue
import os
from flask import Flask
from flask_restful import Resource, Api, reqparse

STACK = queue.LifoQueue(maxsize=0)
DATA_RETENTION_FILE = r'./data_file'


def write_new_data_to_file(data: str):
    """Add a string to the data retention file"""
    with open(DATA_RETENTION_FILE, 'a') as file:
        file.write(f'{data}\n')


def remove_last_data_from_file():
    """Remove the last data (A.K.A Pop) from the data retention file"""
    # os.system(f'sed -i "$ d" {DATA_RETENTION_FILE}')
    with open(DATA_RETENTION_FILE, mode='r', encoding='utf-8') as file:
        data = file.readlines()
    with open(DATA_RETENTION_FILE, mode='w', encoding='utf-8') as file:
        file.writelines(data[:-1])


def empty_data_from_file():
    """
    Overwrite the data retention file and write nothing to it
    Essentially - clears the file
    """
    open(DATA_RETENTION_FILE, 'w').close()


class Stack(Resource):
    """Adds stack Push and Pop operations using GET and POST (respectively) requests"""
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('data', type=str)

    @staticmethod
    def get():
        """Get the top item from the stack"""
        global STACK
        try:
            remove_last_data_from_file()
            return STACK.get_nowait(), 200
        except queue.Empty:
            return 'No more items in stack', 403

    def post(self):
        """Push new item to the top of the stack"""
        global STACK
        try:
            args = self.parser.parse_args()
            if args['data'] is None:
                return "Missing parameter with key 'data' from request.", 400
            STACK.put_nowait(args['data'])
            write_new_data_to_file(args['data'])
            return 'Operation completed.', 201
        except queue.Full:
            return 'Stack is full, no space left.', 403

    @staticmethod
    def put():
        """Specifically deny PUT requests"""
        return 'Method not allowed', 405

    @staticmethod
    def delete():
        """Specifically deny DELETE requests"""
        return 'Method not allowed', 405


class StackPeek(Resource):
    """Adds stack Peek operation using GET request"""
    @staticmethod
    def get():
        """Peek to the top item of the stack"""
        global STACK
        try:
            top_item = STACK.get_nowait()
            STACK.put_nowait(top_item)
            return top_item, 200
        except queue.Empty:
            return 'Stack is empty. Nothing to see here.', 404

    @staticmethod
    def post():
        """Specifically deny POST requests"""
        return 'Method not allowed', 405

    @staticmethod
    def put():
        """Specifically deny PUT requests"""
        return 'Method not allowed', 405

    @staticmethod
    def delete():
        """Specifically deny DELETE requests"""
        return 'Method not allowed', 405


class StackReverse(Resource):
    """Adds stack Revert operation using GET request"""
    @staticmethod
    def get():
        """Revert the stack's order"""
        global STACK
        empty_data_from_file()
        temp_queue = queue.LifoQueue(maxsize=0)
        while not STACK.empty():
            item = STACK.get_nowait()
            temp_queue.put_nowait(item=item)
            write_new_data_to_file(item)
        STACK = temp_queue
        return 'Operation completed', 200

    @staticmethod
    def post():
        """Specifically deny POST requests"""
        return 'Method not allowed', 405

    @staticmethod
    def put():
        """Specifically deny PUT requests"""
        return 'Method not allowed', 405

    @staticmethod
    def delete():
        """Specifically deny DELETE requests"""
        return 'Method not allowed', 405


def initialize_stack():
    """
    Try reading a data file and build stack content from it
    Create said file if doesn't exist
    """
    global STACK
    global DATA_RETENTION_FILE
    try:
        with open(DATA_RETENTION_FILE, 'r') as file:
            data_cnt = 0
            for line in file:
                STACK.put_nowait(line.strip())
                data_cnt += 1
            print(f'Stack created with {data_cnt} items')
    except FileNotFoundError:
        # Create the data retention file if does not already exists
        open(DATA_RETENTION_FILE, 'w').close()


def create() -> Flask:
    """Create a new flask application to operate a stack via REST web API"""
    app = Flask(__name__)
    api = Api(app)
    api.add_resource(Stack, '/stack/')
    api.add_resource(StackPeek, '/stack/peek/')
    api.add_resource(StackReverse, '/stack/revert/')
    return app


if __name__ == '__main__':
    initialize_stack()
    my_app = create()
    my_app.run(debug=False, port=5000)
