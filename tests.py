"""
Tests module for my stack implementation using Flask and REST web API
"""

import unittest
from flask import json
import create_app


class Testing(unittest.TestCase):
    def push(self, data: str):
        """
        GIVEN a Flask application
        WHEN the '/stack/' page is requested (POST) with 'data' parameter
        THEN check that response is valid
        """
        with self.stack_app.test_client() as test_client:
            response = test_client.post('/stack/', headers={'Content-Type': 'application/json'}, data=json.dumps({'data': data}))
            self.assertEqual(response.status_code, 201)
            self.assertIn(b'Operation completed', response.data)

    def get(self, data: str):
        """
        GIVEN a Flask application
        WHEN the '/stack/' page is requested (GET)
        THEN check that response is valid
        """
        with self.stack_app.test_client() as test_client:
            response = test_client.get('/stack/', headers={'Content-Type': 'application/json'})
            self.assertEqual(response.status_code, 200)
            self.assertIn(data, response.data.decode('utf-8'))

    def peek(self, data: str):
        """
        GIVEN a Flask application
        WHEN the '/stack/peek' page is requested (GET)
        THEN check that response is valid
        """
        with self.stack_app.test_client() as test_client:
            response = test_client.get('/stack/peek/', headers={'Content-Type': 'application/json'})
            self.assertEqual(response.status_code, 200)
            self.assertIn(data, response.data.decode('utf-8'))

    def revert(self):
        """
        GIVEN a Flask application
        WHEN the '/stack/revert' page is requested (GET)
        THEN check that response is valid
        """
        with self.stack_app.test_client() as test_client:
            response = test_client.get('/stack/revert/', headers={'Content-Type': 'application/json'})
            self.assertEqual(response.status_code, 200)

    def test_push_n_pop(self):
        """Push data to stack, make sure the same data returns after pop"""
        data = 'test string'
        self.stack_app = create_app.create()
        self.push(data)
        self.get(data)

    def test_peek(self):
        """
        Push data to stack
        Peek to stack and verify data correctness
        Pop from stack to make sure data was not lost during peek operation
        """
        data = 'my test string'
        self.stack_app = create_app.create()
        self.push(data)
        self.peek(data)
        self.get(data)

    def test_reverse(self):
        """
        Push list of data to stack
        Reverse the stack's order
        Pop all items from stack and validate the order was reversed and no items were lost
        """
        data = ['abc', 'ABC', '123']
        self.stack_app = create_app.create()
        for item in data:
            self.push(item)
        self.revert()
        for item in data:
            self.get(item)


if __name__ == '__main__':
    unittest.main()
